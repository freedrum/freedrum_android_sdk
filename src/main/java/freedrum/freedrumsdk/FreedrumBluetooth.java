package freedrum.freedrumsdk;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.midi.MidiDeviceInfo;
import android.media.midi.MidiManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.ArraySet;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import no.nordicsemi.android.dfu.DfuServiceController;
import no.nordicsemi.android.dfu.DfuServiceInitiator;

import static android.media.midi.MidiDeviceInfo.PROPERTY_BLUETOOTH_DEVICE;

/**
 * Created by august on 4/7/17.
 */

public class FreedrumBluetooth {
    private boolean mScanning;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private Activity context;

    static private FreedrumBluetooth inst;
    private DfuServiceController dfuServiceController;
    private ParcelUuid dfuServiceUuid = new ParcelUuid(DEFAULT_DFU_SERVICE_UUID);
    private int zipFile;
    private FreedrumSensor dfuPendingStick;
	private final MidiManager midiManager;

	public static FreedrumBluetooth instance(Activity activity) throws CantStartMidiException {
        if (inst==null)
            inst=new FreedrumBluetooth(activity);
        if (activity!=null)
            inst.context=activity;
        return inst;
    }

    @NotNull
    public BluetoothDevice getRemoteDevice(@Nullable String address) {
        return mBluetoothAdapter.getRemoteDevice(address);
    }

    public static class CantStartMidiException extends Exception{

    }
    private FreedrumBluetooth(Activity context) throws CantStartMidiException {
        this.context = context;

        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();


		midiManager = midiManager(context);
		if (midiManager!=null) {

            midiManager.registerDeviceCallback(new MidiManager.DeviceCallback() {
                @Override
                public void onDeviceAdded(MidiDeviceInfo device) {
                    super.onDeviceAdded(device);
                }

                @Override
                public void onDeviceRemoved(MidiDeviceInfo device) {
                    super.onDeviceRemoved(device);

                    BluetoothDevice bluetoothDevice = (BluetoothDevice) device.getProperties().get(PROPERTY_BLUETOOTH_DEVICE);
                    if (bluetoothDevice != null) {
                        FreedrumSensor freedrumSensor = getDrumstick(bluetoothDevice.getAddress());
                        if (freedrumSensor != null) {
                            freedrumSensor.onDisconnected();
                        }

                    }
                }
            }, null);
        }
        else{
		    Log.w(TAG,"Can't get MidiManager");
        }
    }

    static MidiManager midiManager(Context context){
	    return (MidiManager) context.getSystemService(Context.MIDI_SERVICE);
    }
    public boolean hasMidi(){
	    return midiManager!=null;
    }

    public boolean removeBond(BluetoothDevice btDevice)
            throws Exception
    {
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        Boolean returnValue = (Boolean) removeBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }
    public void startDFU(String address, int zipFile)
    {

        FreedrumSensor drumstick = getDrumstick(address);
        drumstick.putSensorIntoBootLoaderMode();

        dfuPendingStick=drumstick;
        this.zipFile=zipFile;
        dfuPending=true;
        startScan();
    }

    public void abortDFU(){
        if (dfuServiceController!=null)
            dfuServiceController.abort();
        dfuPending=false;
    }
    public boolean isDfuPending(){
        return dfuPending;
    }
    public void stopScan() {
        if (bluetoothLeScanner!=null)
            bluetoothLeScanner.stopScan(scanCallback);

    }

    class CallbackDispatcher implements FreedrumBluetoothCallback{
        Set<FreedrumBluetoothCallback> listeners=new HashSet<>();

        public void addCallback(FreedrumBluetoothCallback cb){
            listeners.add(cb);
        }
        public void removeCallback(FreedrumBluetoothCallback cb){
            listeners.remove(cb);
        }


        @Override
        public void stickFound(FreedrumSensor freedrumSensor) {
            for (FreedrumBluetoothCallback listener : listeners) {
                listener.stickFound(freedrumSensor);
            }
        }

        @Override
        public void scanFailed(int errorCode){
                for (FreedrumBluetoothCallback listener : listeners) {
                    listener.scanFailed(errorCode);
                }
            }
        @Override
        public void freedrumBluetoothSensorReadyForDFU(String address, String name) {
            for (FreedrumBluetoothCallback listener : listeners) {
                listener.freedrumBluetoothSensorReadyForDFU(address,name);
            }
        }
    }
    CallbackDispatcher callbackDispatcher=new CallbackDispatcher();
    public void removeCallback(FreedrumBluetoothCallback cb){
        callbackDispatcher.removeCallback(cb);
    }
    public void addCallback(FreedrumBluetoothCallback cb){
        callbackDispatcher.addCallback(cb);
    }
    private boolean dfuPending;
    final ScanCallback scanCallback = new ScanCallback() {

        @Override
        public void onScanFailed(int errorCode) {
            switch (errorCode){
                case ScanCallback.SCAN_FAILED_ALREADY_STARTED:

                break;
            }
            callbackDispatcher.scanFailed(errorCode);
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            ScanRecord scanRecord = result.getScanRecord();

//                List<ParcelUuid> serviceUuids = scanRecord.getServiceUuids();

            BluetoothDevice device = result.getDevice();
            if (scanRecord.getServiceUuids().contains(dfuServiceUuid)){
                if (dfuPending){
                    dfuPending=false;
                    dfuPendingStick.disconnect(true);
                    try {
                        removeBond(dfuPendingStick.getDevice());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dfuPendingStick=null;


                    String deviceAddress = device.getAddress();

                    startDfuNow(deviceAddress,zipFile);
                }

                callbackDispatcher.freedrumBluetoothSensorReadyForDFU(device.getAddress(),device.getName());
            }
            else {
                FreedrumSensor freedrumSensor;
                if (!deviceList.contains(device)) {
                    deviceList.add(device);
                    freedrumSensor = new FreedrumSensorBLE(device, context);

                    drumsticks.put(device.getAddress(), freedrumSensor);
                } else
                    freedrumSensor = drumsticks.get(device.getAddress());
                callbackDispatcher.stickFound(freedrumSensor);

                freedrumSensor.setRssiVal(result.getRssi());
                freedrumSensor.setRssiTime(System.currentTimeMillis());
            }
        }
    };

    public void startDfuNow(String deviceAddress,int zip) {
        DfuServiceInitiator dfuServiceInitiator = new DfuServiceInitiator(deviceAddress);
        dfuServiceInitiator.setDeviceName("Freedrum");
        dfuServiceInitiator.setKeepBond(false);
        dfuServiceInitiator.setZip(zip);
        Log.d(TAG, "starting dfu");

        dfuServiceController = dfuServiceInitiator.start(context, DfuService.class);
    }

    static final String TAG="freedrum";
    /**
     * start scan
     *
     * @return 0 if scanning was started. -1 if we had no permission to do it.
     */
    public int startScan() {

        if (!mBluetoothAdapter.isEnabled()) {
            callbackDispatcher.scanFailed(FreedrumBluetoothCallback.SCAN_FAILED_BLUETOOTH_OFF);
            return -1;
        }
        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        String coarseLocation = "android.permission.ACCESS_COARSE_LOCATION";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(coarseLocation) != PackageManager.PERMISSION_GRANTED) {
				final String[] permissions = {coarseLocation};
				context.requestPermissions(permissions, 1);
				return -1;
			}
        }


        mScanning = true;


        ScanFilter scanFilter = new ScanFilter.Builder().setServiceUuid(freedrum_service__uuid).build();
        //bluetoothLeScanner.startScan(scanCallback);
        ScanFilter dfuScanFilter = new ScanFilter.Builder().setServiceUuid(dfuServiceUuid).build();


        bluetoothLeScanner.startScan(Arrays.asList(scanFilter,dfuScanFilter), new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build(), scanCallback);
        return 0;
    }
	protected static final UUID DEFAULT_DFU_SERVICE_UUID       = new UUID(0x0000FE5900001000L, 0x800000805F9B34FBL); // 16-bit UUID assigned by Bluetooth SIG
    public FreedrumSensor getDrumstick(String address) {
        FreedrumSensor freedrumSensor = drumsticks.get(address);
        if (freedrumSensor ==null) {

            BluetoothDevice remoteDevice = mBluetoothAdapter.getRemoteDevice(address);
            deviceList.add(remoteDevice);
            freedrumSensor = new FreedrumSensorBLE(remoteDevice, context);
            drumsticks.put(address, freedrumSensor);
        }
        return freedrumSensor;
    }

    List<BluetoothDevice> deviceList = new ArrayList<>();
    HashMap<String,FreedrumSensor> drumsticks=new HashMap<>();

    public Collection<FreedrumSensor> getSensors(){
        return drumsticks.values();
    }
    private static final long SCAN_PERIOD = 1000 * 60;
    private Handler mHandler;


	final static ParcelUuid apple_midi_service_uuid = ParcelUuid.fromString("03b80e5a-ede8-4b33-a751-6ce34ec4c700");
	static ParcelUuid freedrum_service__uuid = ParcelUuid.fromString("0e5a1523-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_char_orientation = ParcelUuid.fromString("0e5a1525-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_drum_conf = ParcelUuid.fromString("0e5a1526-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_version_char = ParcelUuid.fromString("0e5a1527-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_status_uuid = ParcelUuid.fromString("0e5a1528-ede8-4b33-a751-6ce34ec47c00");

	final static ParcelUuid apple_midi_io_char=ParcelUuid.fromString("7772e5db-3868-4112-a1a9-f2669d106bf3");


    public static final UUID BATTERY_SERVICE_UUID =
            UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    public static final UUID BATTERY_LEVEL_CHARACTER_UUID =
            UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
}
