package freedrum.freedrumsdk;


import android.bluetooth.BluetoothDevice;

/**
 * Created by fredrik on 2017-06-07.
 */
public interface FreedrumBluetoothCallback {
    void stickFound(FreedrumSensor freedrumSensor);

    /**
     * Fails to start scan as BLE scan with the same settings is already started by the app.
     */
    public static final int SCAN_FAILED_ALREADY_STARTED = 1;

    /**
     * Fails to start scan as app cannot be registered.
     */
    public static final int SCAN_FAILED_APPLICATION_REGISTRATION_FAILED = 2;

    /**
     * Fails to start scan due an internal error
     */
    public static final int SCAN_FAILED_INTERNAL_ERROR = 3;

    /**
     * Fails to start power optimized scan as this feature is not supported.
     */
    public static final int SCAN_FAILED_FEATURE_UNSUPPORTED = 4;

    /**
     * Fails to start scan as it is out of hardware resources.
     *
     * @hide
     */
    public static final int SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES = 5;

    public static final int SCAN_FAILED_BLUETOOTH_OFF = -1;

    void scanFailed(int errorCode);
    void freedrumBluetoothSensorReadyForDFU(String address,String name);
}
