package freedrum.freedrumsdk

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.midi.MidiDevice
import android.media.midi.MidiInputPort
import android.media.midi.MidiManager
import android.media.midi.MidiOutputPort
import android.media.midi.MidiReceiver
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.content.LocalBroadcastManager
import android.util.Log

import java.io.ByteArrayInputStream
import java.io.IOException
import java.util.regex.Matcher
import java.util.regex.Pattern

import freedrum.freedrumsdk.FreedrumBluetooth.TAG
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import kotlin.experimental.and
import kotlin.experimental.or

/**
 * Created by august on 4/7/17.
 */

open class FreedrumSensor(val device: BluetoothDevice, internal val context: Context) {

    var rssiTime: Long = 0
    var rssiVal: Int = 0
    internal val LOG_TAG = "drumstick"
    internal open fun useMidi(): Boolean {
        val m = FreedrumBluetooth.midiManager(context)
        return if (m!=null) true else return false
    }


    private fun closeMidi() {
        try {
            if (midiInputPort != null)
                midiInputPort!!.close()
            if (midiDevice != null)
                midiDevice!!.close()
            midiDevice = null
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    internal val localBroadcastManager: LocalBroadcastManager
    private var midiInputPort: MidiInputPort? = null
    private var timeLastSentCC = System.nanoTime()
    private val milliSecondsDelayBetweenCCs = 10
    private val nsToMs = 1000000

    internal fun processCommandCue() {
        val timeCurrentCCAttempt = System.nanoTime()

        // If the current attempt is too close to the last CC we sent wait to not clog up the pipe
        if( timeCurrentCCAttempt  - timeLastSentCC < nsToMs * milliSecondsDelayBetweenCCs ){
            return
        }
        timeLastSentCC = timeCurrentCCAttempt

        synchronized(commandCue) {
            val cmd = commandCue.poll()
            if (cmd != null) {
                log("decueing midi write")
                cmd.run()

            }
        }
    }

    var version: String? = null
        internal set
    internal var battLevel: Int = 100
    var handler: Handler? = null

    val address: String
        get() = device.address

    override fun toString(): String {
        return device.name
    }

    internal val sensorDelegates: MutableSet<FreedrumSensorDelegate> = mutableSetOf<FreedrumSensorDelegate>()

    init {

        localBroadcastManager = LocalBroadcastManager.getInstance(context)
        val d = globalDelegate
        if (d != null) {
            sensorDelegates += d
        }
    }

    fun subscribeOrientation(enable: Boolean) {}


    open val isConnected: Boolean
        get() = midiDevice != null


    open fun connect(bond: Boolean) {
        if (isConnected) {
            Log.w(TAG, "Already connected!")
            return
        }
        log("connecting to " + device.name)

        if (bond && device.bondState == BluetoothDevice.BOND_NONE) {

            context.registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val device = intent.getStringExtra(BluetoothDevice.EXTRA_NAME)
                    val state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1)
                    if (BluetoothDevice.BOND_BONDED == state) {
                        context.unregisterReceiver(this)
                        connect(bond)
                    }
                }
            }, IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED))


            device.createBond()
            return //we'll get called again when bonding completes
        }

        if (!connectMidi())
            throw FreedrumBluetooth.CantStartMidiException()


    }

    internal var midiDevice: MidiDevice? = null
    @RequiresApi(api = Build.VERSION_CODES.M)
    private fun connectMidi(): Boolean {
        val m = FreedrumBluetooth.midiManager(context) ?: return false

        m.openBluetoothDevice(device, { device ->
            if (midiDevice != null) {
                Log.w(TAG, "Already open? Closing old midi device.")
                try {
                    midiDevice!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            midiDevice = device

            val midiOutputPort = device.openOutputPort(0)

            midiOutputPort.connect(object : MidiReceiver() {
                @Throws(IOException::class)
                override fun onSend(msg: ByteArray, offset: Int, count: Int, timestamp: Long) {
                    handleRawMidi(msg, offset,count)
                }
            })


            this@FreedrumSensor.version = parseDeviceName(this@FreedrumSensor.device.name)


            midiInputPort = device.openInputPort(0)
            log("midi input: " + midiInputPort!!)
            pendingWrites = 0
            commandCue.clear()

            // Go through all listeners and tell them we are connected and about the current version
            sensorDelegates.forEach {
                it.connected(this)
                if (version!=null)
                   it.freedrumVersionReport(this@FreedrumSensor, version)
            }

            val intent = Intent("sensorConnected")
            intent.putExtra("address", address)
            localBroadcastManager.sendBroadcast(intent)

            if (version==null) {
                Log.d(LOG_TAG,"Sensor has been renamed, asking for version via MIDI")
                // We need to give the connection some time to establish a outwards
                // channel - this cannot be done to early or it will fail
                // we currently don't have a message knowing when the time is right but 5000
                // works with a fair ammount of margin
                handler?.postDelayed({
                    // Send a READ request to the Sensor
                    writeCC(0, FreedrumCC.read_value.toInt(), FreedrumCC.version)
                }, 5000)
            }
        }, null)

        return true
    }

    internal fun handleRawMidi(msg: ByteArray, offset: Int,len:Int) {

        var pos = offset
        val command = (0xf0.toByte() and (msg[pos])).toByte()
        val channel = 0xf.toByte() and (msg[pos])
        pos++
        when (command) {
            MidiConstants.STATUS_NOTE_ON -> {
                val note = msg[pos++] and 0xff.toByte()
                val velocity = msg[pos++] and 0xff.toByte()
                log("Got note on! " + note)
                sensorDelegates.forEach { it.freedrumSensorNoteOn(this@FreedrumSensor, note.toInt(), velocity.toInt()) }
                val intent = Intent("midiNote")
                intent.putExtra("note", note)
                intent.putExtra("velocity", velocity)
                intent.putExtra("address", address)
                localBroadcastManager.sendBroadcast(intent)
                processCommandCue()

            }
            MidiConstants.STATUS_CONTROL_CHANGE -> {

                val cc = msg[pos++] and 0xff.toByte()
                val value = msg[pos++] and 0xff.toByte()
                sensorDelegates.forEach { it.freedrumSensorCC(this@FreedrumSensor, channel.toInt(),cc.toInt(), value.toInt()) }
                log("got cc:$cc, val: $value")
                synchronized(commandCue) {
                    when (cc.toByte()) {
                        FreedrumCC.status -> {

                            pendingWrites--
                            if (pendingWrites < 0) {
                                log("Strange, more statuses than sent commands...?")
                                pendingWrites = 0
                            }
                        }

                        FreedrumCC.battery_level -> {
                            sensorDelegates.forEach { it.freedrumSensorBatteryReport(this, value.toInt()) }
                            battLevel = value.toInt()
                        }

                        FreedrumCC.midiNoteForPad -> {
                            zoneCache[channel.toInt()]=value.toInt()
                        }
                    }


                    when (cc.toByte()) {
                        FreedrumCC.battery_level -> {

                        }
                        else -> {
                            cancelTimeout()
                        }
                    }

                    //									processCommandCue();


                    processCommandCue()
                }
            }
            MidiConstants.STATUS_SYSTEM_EXCLUSIVE -> {
                log("got sysex!")
                when (msg[pos]){
                    FreedrumCC.version.toByte() -> {
                        val length = len - (pos - offset) - 1//-1 to remove the trailing sysex end marker
                        val vers=String(msg,pos+1, length)
                        version=vers
                        sensorDelegates.forEach { it.freedrumVersionReport(this, version) }
                    }
                }


            }
        }
        //		processCommandCue();

    }

    fun cancelTimeout(){
        commandTimeoutTask?.cancel()
        commandTimeoutTask=null
    }

    fun turnOff() {
        sendMidiSysex(byteArrayOf(0x5))
    }

    inner class Status {
        var compassValid: Boolean = false
        var isStill: Boolean = false
    }

    internal var currentStatus = Status()

    open fun disconnect(close: Boolean) {

        closeMidi()


        //bluetoothGatt.close();

    }

    fun onDisconnected() {
        val intent = Intent("sensorDisconnected")
        intent.putExtra("address", address)
        localBroadcastManager.sendBroadcast(intent)
        sensorDelegates.forEach { it.disconnected(this) }

        closeMidi()
    }

    fun setScantime(scantime: Int) {

    }


    /**
     * this is a cue of commands due to be sent, but we've got to wait for ack from the device first.
     */
    internal var commandCue: Queue<Runnable> = ArrayDeque()


    internal fun log(s: String) {

        Log.d(LOG_TAG, s)
    }

    public object FreedrumCC {
        val command: Byte = 20
        val status: Byte = 21
        val read_value: Byte = 22
        val y_pos: Byte = 23
        val battery_level: Byte = 24
        val z_pos: Byte = 103
        val threshold: Byte = 104
        val sensitivity: Byte = 105
        val midiNoteForPad: Byte = 106
        val x_axis_cc: Byte = 107
        //if >0 orientation changes around the x-axis are sent as CC <x_axis_cc>
        val y_axis_cc: Byte = 108
        val z_axis_cc: Byte = 109
        val refDrumWindowSize: Byte = 110
        val refDrumStrength: Byte = 111
        val midiNoteForPadLeftTwist=112
        /*
         * How accurately do we need to hit the ref drum in y-axis to get prediction of hit time? 0 means turn off this kind
         * of prediction.
         */
        val predictVarianceThreshold=113
        /**
         * what cc to send for foot pedal change. Foot pedal cc is sent for the ref drum when y is above the pad mid y.
         */
        val footPedalCC=114
        val version=115
        val zoneChange:Byte=16

        val command_save_settings: Byte = 0
    }


    internal fun parseIncoming(value: ByteArray) {
        val inp = ByteArrayInputStream(value, 1, value.size - 1)


    }

    @Transient internal var pendingWrites = 0
    open fun sendMIDI(buffer: ByteArray, size: Int) {


        if (!isConnected) {
            Log.w(TAG, "Cant send midi, device closed")
            return
        }

            if (midiInputPort == null) {
                Log.e(TAG, "sendMIDI: midiInputPort is null, aborting!")
                return
            }
            try {
                synchronized(commandCue) {
                    if (pendingWrites > 0) {
                        log("encuing midi write")
                        pendingWrites++
                        commandCue.add(Runnable {
                            try {
                                midiInputPort!!.send(buffer, 0, size)

                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        })
                    } else {
                        pendingWrites++
                        midiInputPort!!.send(buffer, 0, size)
                    }

                }


            } catch (e: IOException) {
                e.printStackTrace()
            }

            return



    }

    var commandTimeoutTask:TimerTask?=null

    fun writeCC(channel: Int, cc: Int, value: Int): Boolean {
        log("Writing CC ${cc}=${value}")
        val buffer = ByteArray(5)
        var numBytes = 0

        if (commandTimeoutTask==null){
            commandTimeoutTask=object :TimerTask(){
                override fun run() {
                    Log.w(LOG_TAG,"Sensor not responding!")
                    commandTimeoutTask=null
                    handler?.post({
                        sensorDelegates.forEach { it.freedrumSensorCantWrite(this@FreedrumSensor) }
                    })
                }
            }
            timer.schedule(commandTimeoutTask,2000)
        }

        buffer[numBytes++] = (MidiConstants.STATUS_CONTROL_CHANGE or channel.toByte())
        buffer[numBytes++] = cc.toByte()
        buffer[numBytes++] = value.toByte()

        when (cc.toByte()){
            FreedrumCC.midiNoteForPad -> {
                zoneCache[channel]=value
            }
        }
        sendMIDI(buffer, numBytes)

        return true

    }


    fun putSensorIntoBootLoaderMode(): Boolean {
        val buffer = ByteArray(32)
        var numBytes = 0

        //sysex version
        if (useMidi()) {
            buffer[numBytes++] = 0xf0.toByte()
            buffer[numBytes++] = 0x04.toByte()
            buffer[numBytes++] = 0xf7.toByte()
        } else {
            buffer[numBytes++] = 0xf4.toByte()
            buffer[numBytes++] = 0xf4.toByte()
        }
        sendMIDI(buffer, numBytes)
        //midiInputPort.send(buffer, 0, numBytes);
        return true


    }

    internal fun sendMidiSysex(msg: ByteArray) {
        val buffer = ByteArray(msg.size + 2)
        buffer[0] = 0xf0.toByte()
        for (i in msg.indices)
            buffer[1 + i] = msg[i]
        buffer[msg.size + 1] = 0xf7.toByte()
        sendMIDI(buffer, buffer.size)
    }

    val battery: Int
        get() {
           // writeCC(0, FreedrumCC.read_value.toInt(), FreedrumCC.battery_level.toInt())
            return battLevel
        }

    fun setNote(note: Int, forPad: Int): Boolean {
        log("set note $note for pad $forPad")
        return writeCC(forPad, FreedrumCC.midiNoteForPad.toInt(), note)
    }
    fun setNoteLeftTwist(note: Int, forPad: Int): Boolean {
        log("set note $note for pad $forPad")
        return writeCC(forPad, FreedrumCC.midiNoteForPadLeftTwist.toInt(), note)
    }

    fun saveConfig(): Boolean {
        return writeCC(0, FreedrumCC.command.toInt(), FreedrumCC.command_save_settings.toInt())
    }

    fun addCallback(delegate: FreedrumSensorDelegate) {
        if (sensorDelegates.contains(delegate)) {
            Log.w(TAG, "Can't add the same delegate twice!")
            return
        }
        sensorDelegates += delegate
    }

    fun removeCallback(delegate: FreedrumSensorDelegate) {
        if (!sensorDelegates.contains(delegate)) {
            Log.w(TAG, "Can't remove that callback dude!")
            return
        }
        sensorDelegates -= delegate

    }

    fun setSensitivity(`val`: Byte) {
        writeCC(0, FreedrumCC.sensitivity.toInt(), `val`.toInt())
    }

    val versionNumber: Int
        get() {
            if (version == null) {
                return -1
            }
            val matcher = versionPattern.matcher(version!!)
            return if (matcher.find()) {
                Integer.decode(matcher.group(1))!!
            } else
                -1
        }

    companion object {
        val timer = Timer()
        internal fun parseDeviceName(name: String): String? {
            val namePattern = Pattern.compile("FD1 (.+)")
            val matcher = namePattern.matcher(name)
            return if (!matcher.matches()) null else matcher.group(1)
        }

        internal val versionPattern = Pattern.compile("FD1 v(.+)")
        /**
         * this one gets called for all sensors.
         */
        var globalDelegate: FreedrumSensorDelegate? = null
    }
    val angleScale = 360.0f / 127;
    fun scaleAngle(x:Int)=if (x>=0) (x/angleScale).toInt() else 127+(x/angleScale).toInt()
    fun sendPadPos(padNr: Int, y: Int, z:Int) {

        writeCC(padNr, FreedrumCC.y_pos.toInt(),scaleAngle(y))
        writeCC(padNr, FreedrumCC.z_pos.toInt(),scaleAngle(z))

    }

    fun unpairDevice() {
        try {
            val method = device.javaClass.getMethod("removeBond")
            method.invoke(device)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    val zoneCache=HashMap<Int,Int>()
    fun getNote(zone:Int): Int? {
        if (zoneCache.containsKey(zone))
            return zoneCache[zone]

        writeCC(zone, FreedrumCC.read_value.toInt(), FreedrumCC.midiNoteForPad.toInt())
        return null
    }

}
