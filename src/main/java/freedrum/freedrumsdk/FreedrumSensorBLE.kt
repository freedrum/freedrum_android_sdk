package freedrum.freedrumsdk

import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created by august on 9/20/17.
 */

class FreedrumSensorBLE(device: BluetoothDevice, context: Context) : FreedrumSensor(device, context) {


    /**
     * BLE stuff below!
     */
    private var bluetoothGatt: BluetoothGatt? = null
    private var midiChar: BluetoothGattCharacteristic? = null



    private var closeOnDisconnect: Boolean = false
    inner class GattCallback: BluetoothGattCallback() {
        override fun onCharacteristicWrite(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)

            processCommandCue()
            pendingWrites--
            if (pendingWrites < 0) {
                Log.e(FreedrumBluetooth.TAG, "processCommandCue error!")
                pendingWrites = 0
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            processCommandCue()
        }

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int,
                                             newState: Int) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                if (versionCh != null) {
                    //reconnect
                    connectServices(gatt)

                } else
                    gatt.discoverServices()
                log("Connected")

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                sensorDelegates.forEach {
                    it.disconnected(this@FreedrumSensorBLE)
                }
                log("Disconnected")
                if (closeOnDisconnect) {
                    bluetoothGatt?.close()
                    versionCh = null
                    midiChar = null
                    bluetoothGatt = null
                }

                val intent = Intent("sensorDisconnected")
                intent.putExtra("address", address)
                localBroadcastManager.sendBroadcast(intent)

            }
            pendingWrites = 0
            commandCue.clear()
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int) {

            if (status == BluetoothGatt.GATT_SUCCESS)
                handleCharUpdate(characteristic)

            processCommandCue()
            super.onCharacteristicRead(gatt, characteristic, status)
        }

        private fun handleCharUpdate(characteristic: BluetoothGattCharacteristic) {


            if (characteristic.uuid == FreedrumBluetooth.BATTERY_LEVEL_CHARACTER_UUID) {
                battLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)
                log("battery level: " + battLevel)
                sensorDelegates.forEach({
                    it.freedrumSensorBatteryReport(this@FreedrumSensorBLE, battLevel)
                })
            } else if (characteristic === midiChar) {
                val value = characteristic.value

                handleRawMidi(value, 2,value.size-2)
            }


        }

        internal var versionCh: BluetoothGattCharacteristic? = null

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    connectServices(gatt)

                }
            }
        }

        private fun connectServices(gatt: BluetoothGatt) {
            run {
                val service = gatt.getService(FreedrumBluetooth.freedrum_service__uuid.uuid)
                versionCh = service.getCharacteristic(FreedrumBluetooth.freedrum_version_char.uuid)

                val battservice = gatt.getService(FreedrumBluetooth.BATTERY_SERVICE_UUID)
                val battCh = battservice.getCharacteristic(FreedrumBluetooth.BATTERY_LEVEL_CHARACTER_UUID)

                if (!useMidi()) {
                    val midiService = gatt.getService(FreedrumBluetooth.apple_midi_service_uuid.uuid)
                    midiChar = midiService.characteristics[0]
                }


                gatt.readCharacteristic(versionCh)
                commandCue.add(Runnable { gatt.readCharacteristic(battCh) })

                //				gatt.beginReliableWrite();
                subscribeChar(battCh, true)
                    subscribeChar(midiChar, true)
                commandCue.add(Runnable { gatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH) })

            }

            sensorDelegates.forEach {
                it.connected(this@FreedrumSensorBLE)
                it.freedrumVersionReport(this@FreedrumSensorBLE, version)
            }
            val intent = Intent("sensorConnected")
            intent.putExtra("address", address)
            localBroadcastManager.sendBroadcast(intent)

        }


        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {

            handleCharUpdate(characteristic)

        }
    }
    override val isConnected: Boolean
        get() {
            if (useMidi())
                return midiDevice!=null

            val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

            return bluetoothGatt != null && BluetoothProfile.STATE_CONNECTED == bluetoothManager.getConnectionState(device, BluetoothProfile.GATT_SERVER)

        }
    override fun disconnect(close: Boolean) {
        super.disconnect(close)
        closeOnDisconnect = close

        if (bluetoothGatt != null)
            bluetoothGatt?.disconnect()

    }
    override fun connect(bond: Boolean) {
        try {
            super.connect(bond)
        } catch (e: FreedrumBluetooth.CantStartMidiException) {

            Log.w(FreedrumBluetooth.TAG, "Using raw BLE")
            version = parseDeviceName(device.name)
            if (bluetoothGatt == null) {

                bluetoothGatt = device.connectGatt(context, true, GattCallback())

            } else
                bluetoothGatt?.connect()

        }
    }

    private fun subscribeChar(ch: BluetoothGattCharacteristic?, enable: Boolean) {
        if (ch == null) {
            Log.w(LOG_TAG, "Null char cant be subscribed")
            return
        }
        bluetoothGatt?.setCharacteristicNotification(ch, enable)
        val descriptors = ch.descriptors
        if (descriptors.isEmpty()) {
            Log.e(LOG_TAG, "Can't subscribe to characteric, no descriptors." + ch.toString())
            return
        }
        val descriptor = descriptors[0]
        val runnable = Runnable {
            descriptor.value = if (enable) BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE else BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
            bluetoothGatt?.writeDescriptor(descriptor)
        }

        if (!commandCue.isEmpty())
            commandCue.add(runnable)
        else
            runnable.run()
    }
    override fun sendMIDI(buffer: ByteArray, size: Int) {

        if (useMidi()) {
            super.sendMIDI(buffer, size)
            return
        }

        if (midiChar == null) {
            Log.e(LOG_TAG, "sendMIDI: midichar is null!")
            return
        }

        val b = ByteArray(size + 2)
        b[1] = 0x80.toByte()
        b[0] = b[1]//header
        System.arraycopy(buffer, 0, b, 2, size)
        synchronized(commandCue) {
            if (pendingWrites > 0) {
                log("encuing midi write")
                commandCue.add(Runnable {
                    midiChar?.setValue(b)
                    bluetoothGatt?.writeCharacteristic(midiChar)
                })
            } else {
                midiChar?.setValue(b)
                bluetoothGatt?.writeCharacteristic(midiChar)
            }
            pendingWrites++
        }
    }
}