package freedrum.freedrumsdk;

/**
 * Created by august on 8/13/17.
 */

public interface FreedrumSensorDelegate {
	void freedrumSensorBatteryReport(FreedrumSensor source, int percent);
	void freedrumSensorNoteOn(FreedrumSensor source,int note, int velocity);

	void freedrumSensorCC(FreedrumSensor source, int channel, int cc, int value);

	void freedrumVersionReport(FreedrumSensor sensor, String versionString);

	void disconnected(FreedrumSensor sensor);

	void connected(FreedrumSensor sensor);

	void freedrumSensorCantWrite(FreedrumSensor source);
}
