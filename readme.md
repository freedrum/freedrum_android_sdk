## Adding the SDK to your app

You will need something like this in the top-level build.gradle:

    // Top-level build file where you can add configuration options common to all sub-projects/modules.

    buildscript {
        ext.kotlin_version = '1.1.4-2'
        repositories {
            jcenter()
            maven {
                url "https://maven.google.com"
            }
        }
        dependencies {
            classpath 'com.android.tools.build:gradle:3.0.0-beta6'
            classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
            classpath 'com.google.gms:google-services:3.1.0'
            // NOTE: Do not place your application dependencies here; they belong
            // in the individual module build.gradle files
        }
    }

    allprojects {
        repositories {
            jcenter()
        }
    }

You'll also need a local.properties file, containing the location of your Android SDK, something like this:

    sdk.dir=/home/august/Android/Sdk


If you get an error like this

       - ERROR: /home/august/freedrumsdk/build/intermediates/res/merged/release/values/values.xml:278 invalid dimen

it might be because your machine is running on a non-english locale. A workaround is to do

    export LC_NUMERIC="en_US.UTF-8"

before running gradle. You might also need to issue

    gradle --stop

to stop any previous gradle daemon running with your other locale to get this to work.

## Using the SDK from your code

FreedrumBluetooth is the main class you'll use to access SDK features. You can access it from Java or Kotlin (We're using Kotlin here as a reference because that's what the SDK uses)

    val freedrumBluetooth=FreedrumBluetooth.instance(activity)

To start scanning for Freedrum sensors:

    freedrumBluetooth.addCallback(freedrumCallback)
    freedrumBluetooth.startScan()

where freedrumCallback is an instance of FreedrumBluetoothCallback. When a sensor is found you'll receive a call to "void stickFound(FreedrumSensor freedrumSensor)"

FreedrumSensor represents one Freedrum sensor. You can send MIDI CC's to it, subscribe for updates on MIDI CC's and MIDI notes, connect/disconnect to it etc.

    
    freedrumSensor.addCallback(sensorDelegate)
    freedrumSensor.connect()
    //wait for a call sensorDelegate.connected()

    // channel below is the "zone" to control
    freedrumSensor.writeCC(channel, cc, value)


